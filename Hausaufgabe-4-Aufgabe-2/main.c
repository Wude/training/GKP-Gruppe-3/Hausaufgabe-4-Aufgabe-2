#include "stdafx.h"

#include "util.h"
#include "rational.h"
#include "rational_IO.h"

#include <conio.h>

enum op_code
{
  OpNone,
  OpAddition,
  OpSubtraction,
  OpMultiplication,
  OpDivision,
  OpQuit
};

typedef enum op_code op_code_e;

op_code_e getOperation()
{
  clearScreen();

  printf(
    "Dies ist ein Programm zur Verarbeitung rationaler Zahlen.\n"
    "Folgende Operationen stehen zur Verfuegung:\n"
    "* <A>ddition        (a + b)\n"
    "* <S>ubtraktion     (a - b)\n"
    "* <M>ultiplikation  (a * b)\n"
    "* <D>ivision        (a / b)\n"
    "* <Q>uittieren des Programms (<ESC>)\n"
    "Bitte die gewuenschte Operation ueber ihre zugehoerige Tasten waehlen...\n");

  switch ((char)_getch())
  {
    case 'a':
    case 'A':
      return OpAddition;
    case 's':
    case 'S':
      return OpSubtraction;
    case 'm':
    case 'M':
      return OpMultiplication;
    case 'd':
    case 'D':
      return OpDivision;
    case 'q':
    case 'Q':
    case (char)27:
      return OpQuit;
    default:
      break;
  }

  return OpNone;
}

/******************************************************************************
* Aufgabe 2.5 - Testprogramm
******************************************************************************/
/* Testwerte f�r die Operandeneingabe (mit Zeilenumbr�chen auf die Konsole kopieren):
4
3
2
-1
*/
int main()
{
  rational_t a;
  rational_t b;
  rational_t result;

  for (
    op_code_e op_code = getOperation();
    op_code != OpQuit;
    op_code = getOperation())
  {
    if (op_code != OpNone)
    {
      printf("Bitte die rationale Zahl \"a\" eingeben:\n");
      a = read_rational();
      printf("Bitte die rationale Zahl \"b\" eingeben:\n");
      b = read_rational();
      clearScreen();

      if (op_code == OpAddition)
      {
        printf("Operation: Addition...\n");
        result = rationalAdd(&a, &b);
      }
      else if (op_code == OpSubtraction)
      {
        printf("Operation: Subtraktion...\n");
        result = rationalSubtract(&a, &b);
      }
      else if (op_code == OpMultiplication)
      {
        printf("Operation: Multiplikation...\n");
        result = rationalMultiply(&a, &b);
      }
      else if (op_code == OpDivision)
      {
        printf("Operation: Division...\n");
        result = rationalDivide(&a, &b);
      }

      printf("  a        = ");
      print_rational(a);
      printf("\n  b        = ");
      print_rational(b);
      printf("\n  Ergebnis = ");
      print_rational(result);
      printf("\n");

      pause();
    }
  }

  /**************************************/
  return 0;
}
