#ifndef __RATIONAL_IO_H__
#define __RATIONAL_IO_H__

#include "rational.h"

#include <stdio.h>

/******************************************************************************
* Aufgabe 2.4 - Ein-/Ausgabe: Deklaration
******************************************************************************/

// Eine rationale Zahl von der Konsole lesen.
rational_t read_rational();

// Eine rationale Zahl auf der Konsole ausgeben.
void print_rational(rational_t rational);

#endif
