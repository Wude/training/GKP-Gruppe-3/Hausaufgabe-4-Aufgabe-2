#include "stdafx.h"

#include "rational.h"

/******************************************************************************
* Aufgabe 2.3 - Funktionsimplementationen
******************************************************************************/

// Der gr��te gemeinsame Teiler zweier "long"-Werte.
long greatestCommonDivisor(long a, long b)
{
  long rest;
  while (b != 0.0)
  {
    rest = a % b;
    a = b;
    b = rest;
  }
  return a;
}

// Eine rationale Zahl normalisieren.
void rationalNormalize(rational_t *rational)
{
  // Der Nenner soll vorzeichenfrei bleiben.
  if (rational->denominator < 0)
  {
    rational->numerator = -rational->numerator;
    rational->denominator = -rational->denominator;
  }
  
  // Der soll in Minimalform vorliegen.
  long divisor = greatestCommonDivisor(rational->numerator, rational->denominator);
  if (divisor > 1)
  {
    rational->numerator /= divisor;
    rational->denominator /= divisor;
  }
}

// Zwei rationale Zahlen addieren.
rational_t rationalAdd(rational_t *left, rational_t *right)
{
  rational_t result;
  long divisor = greatestCommonDivisor(left->denominator, right->denominator);

  // Den rechten Nenner verrechnen.
  long denominator = right->denominator / divisor;
  result.numerator = left->numerator * denominator;
  result.denominator = right->denominator / divisor;

  // Den linken Nenner verrechnen.
  denominator = left->denominator / divisor;
  result.numerator += right->numerator * denominator;
  result.denominator *= left->denominator;

  return result;
}

// Zwei rationale Zahlen subtrahieren.
rational_t rationalSubtract(rational_t *left, rational_t *right)
{
  rational_t result;
  long divisor = greatestCommonDivisor(left->denominator, right->denominator);

  // Den rechten Nenner verrechnen.
  long denominator = right->denominator / divisor;
  result.numerator = left->numerator * denominator;
  result.denominator = right->denominator / divisor;

  // Den linken Nenner verrechnen.
  denominator = left->denominator / divisor;
  result.numerator -= right->numerator * denominator;
  result.denominator *= left->denominator;

  return result;
}

// Zwei rationale Zahlen multiplizieren.
rational_t rationalMultiply(rational_t *left, rational_t *right)
{
  rational_t result;
  long divisor = greatestCommonDivisor(left->denominator, right->denominator);

  result.numerator = right->numerator * (left->numerator / divisor);
  result.denominator = right->denominator * (left->denominator  / divisor);

  rationalNormalize(&result);

  return result;
}

// Zwei rationale Zahlen dividieren.
rational_t rationalDivide(rational_t *left, rational_t *right)
{
  rational_t result;
  long divisor = greatestCommonDivisor(left->denominator, right->numerator);

  result.numerator = left->numerator * (right->denominator / divisor);
  result.denominator = left->denominator * (right->numerator / divisor);

  rationalNormalize(&result);

  return result;
}

// Eine rationale Zahl als "double"-Wert liefern.
double rationalToDouble(rational_t *rational)
{
  double result = rational->numerator;
  result /= rational->denominator;
  return result;
}

// Einen "double"-Wert als rationale Zahl liefern.
//rational_t rationalToDouble(double d)
//{
//  rational_t result;
//  return result;
//}
